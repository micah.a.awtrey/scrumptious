from django.urls import path
from recipes.views import show_recipe, recipe_list, create_recipe, edit_recipe, my_recipe_list, add_to_shopping_list, view_shopping_list, remove_from_shopping_list

urlpatterns = [
    path("<int:id>/", show_recipe, name="show_recipe"),
    path("", recipe_list, name="recipe_list"),
    path("create/", create_recipe, name="create_recipe"),
    path("<int:id>/edit/", edit_recipe, name="edit_recipe"),
    path("my_recipes/", my_recipe_list, name="my_recipe_list"),
    path("add/<int:id>", add_to_shopping_list, name="add_to_shopping_list"),
    path("remove/<int:id>/", remove_from_shopping_list, name="remove_from_shopping_list"),
    path("shopping_list/", view_shopping_list, name="view_shopping_list"),
]
