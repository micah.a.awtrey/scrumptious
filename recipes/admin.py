from django.contrib import admin
from recipes.models import Recipe, Steps, Ingredient, ShoppingListItem


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",
    ]

admin.site.register(ShoppingListItem)

@admin.register(Steps)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = [
        "step_number",
        "id",
        "instruction",
    ]

@admin.register(Ingredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display = [
        "amount",
        "id",
        "food_item",
    ]
