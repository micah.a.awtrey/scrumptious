from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe, ShoppingListItem
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required

def redirect_to_recipe_list(request):
    return redirect(request, "recipe_list")

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipe_list = Recipe.objects.all()
    context = {
        "recipe_list": recipe_list
    }
    return render(request, "recipes/list.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("show_recipe", recipe.id)
    else:
        form = RecipeForm

    context = {
        "form": form,
    }

    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            recipe = form.save()
            return redirect("show_recipe", id=id)

    else:
        form = RecipeForm(instance=recipe)

    context = {
        "recipe": recipe,
        "form": form,
    }

    return render(request, "recipes/edit.html", context)

@login_required
def my_recipe_list(request):
    recipe_list = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipe_list
    }
    return render(request, "recipes/list.html", context)

@login_required
def view_shopping_list(request):
    shopping_list = ShoppingListItem.objects.filter(user=request.user)
    context = {
        "shopping_list": shopping_list,
    }
    return render(request, "recipes/shopping_list.html", context)

@login_required
def add_to_shopping_list(request, id):
    recipe_item = Recipe.objects.get(id=id)
    list_item, created = ShoppingListItem.objects.get_or_create(item=recipe_item, user=request.user)
    list_item.quantity += 1
    list_item.save()
    return redirect("view_shopping_list")

@login_required
def remove_from_shopping_list(request, id):
    list_item = ShoppingListItem.objects.get(id=id)
    list_item.delete()
    return redirect("view_shopping_list")
