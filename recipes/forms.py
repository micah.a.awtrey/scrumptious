from django.forms import ModelForm, forms
from recipes.models import Recipe

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "description",
            "picture",
        ]
