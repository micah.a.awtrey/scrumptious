from django.shortcuts import render, redirect
from accounts.forms import SignUpForm, SignInForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User

def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            if password == password_confirmation:
                user = User.objects.create_user(username, None, password, first_name=first_name, last_name=last_name)
                user.save()
                login(request, user)
                return redirect("recipe_list")
            else:
                form.add_error("password", "Passwords do not match.")

    else:
        form = SignUpForm()
    context = {
        "form": form
    }

    return render(request, "accounts/signup.html", context)

def signin(request):
    if request.method == "POST":
        form = SignInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("recipe_list")
            else:
                form.add_error("username", "Invalid login.")

    else:
        form = SignInForm()
        context = {
            "form": form
        }

    return render(request, "accounts/signin.html", context)

def signout(request):
    logout(request)
    return redirect("recipe_list")
